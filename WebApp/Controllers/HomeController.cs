﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Entity;
using IServices;
using Longjubank.FrameWorkCore.Helper;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHolidayService HolidayService;

        public HomeController(IHolidayService service) => HolidayService = service;
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public async Task<IActionResult> Index(string startdate, int days = 1)
        {

            List<ProcDateCountModel> list = new List<ProcDateCountModel>();

            if (string.IsNullOrEmpty(startdate))
            {
                startdate = DateTime.Now.ToString("yyyy-MM-dd");
            }

            list = (await HolidayService.GetProcDateList(startdate, days)).ToList();
            return View(list);
        }

    }
}
