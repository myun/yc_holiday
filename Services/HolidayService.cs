﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Entity;
using IServices;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Longjubank.FrameWorkCore.Helper;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System.Collections;

namespace Services
{
    public class HolidayService : IHolidayService
    {
        private readonly AppDbContext DbContext;
        public HolidayService(AppDbContext appDb) => DbContext = appDb; 

        public async Task<IEnumerable<ProcDateCountModel>> GetProcDateList(string start, int days)
        { 
            MySqlParameter[] param = { new MySqlParameter("@in_begindate",MySqlDbType.VarChar,10),
                new MySqlParameter("@in_days",MySqlDbType.Int32)};
            param[0].Value = start;
            param[1].Value = days;

            var list = await GetTAsync<ProcDateCountModel>("proc_date_count", param, CommandType.StoredProcedure);

            return list;
        }

        private readonly object _conObj = new object();
        public async Task<IEnumerable<T>> GetTAsync<T>(string cmdText, MySqlParameter[] sqlParameter, CommandType cmdType = CommandType.Text) where T : class, new()
        {
            try
            {
                var connection = DbContext.Database.GetDbConnection();

                lock (_conObj)
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                }
                List<T> datalist = new List<T>();
                Type entity = typeof(T);
                System.Reflection.PropertyInfo[] propertylist = entity.GetProperties();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = cmdText;
                    cmd.CommandType = cmdType;
                    cmd.Parameters.AddRange(sqlParameter);

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var item = new T();
                            foreach (var m in propertylist)
                            {
                                if (reader[m.Name] != DBNull.Value)
                                {
                                    m.SetValue(item, reader[m.Name]);
                                }
                            }
                            datalist.Add(item);
                        }
                    }
                } 
                return datalist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
