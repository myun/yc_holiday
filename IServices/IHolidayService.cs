﻿using Entity;
using Longjubank.FrameWorkCore.Helper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IServices
{
    public interface IHolidayService
    {
        Task<IEnumerable<ProcDateCountModel>> GetProcDateList(string start, int days);
    }
}
