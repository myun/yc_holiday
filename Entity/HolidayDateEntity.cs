﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class HolidayDateEntity
    { 
        public string date { get; set; }
        public string month_day { get; set; }
        public string cnday { get; set; }
        public string cnmonth { get; set; }
        public string date_type { get; set; }
        public string date_status { get; set; }
        public string create_time { get; set; }
        public string update_time { get; set; }
    }
}
